# Merge Conflict Checker
This project provides a Bash script that will detect upcoming merge conflicts.

This is accomplished by attempting to merge various branches of a project according to some common branching models/workflows, e.g., GitHub flow, GitLab flow, Gitflow.



## Requirements
- Bash



## Getting Started
Run `./check-conflicts.sh` (without arguments) trigger the help text.

**Example**: `./check-conflicts.sh gitlab.com robr3rd/merge-conflict-checker`



## Sample Output
```
$ cat conflicts.log
Wed May 27 03:56:08 EDT 2020::WARN::owner/project-first::branch1 -> master
Wed May 27 03:56:09 EDT 2020::WARN::owner/project-first::branch2 -> master
Wed May 27 03:56:10 EDT 2020::WARN::owner/project-first::branch3 -> master
Wed May 27 03:56:14 EDT 2020::INFO::owner/project-first::branch2 -> develop
Wed May 27 03:56:15 EDT 2020::INFO::owner/project-first::branch4 -> develop
Wed May 27 03:56:29 EDT 2020::WARN::owner/project-second::branch1 -> master
Wed May 27 03:56:32 EDT 2020::INFO::owner/project-second::branch1 -> develop
Wed May 27 03:56:34 EDT 2020::INFO::owner/project-second::branch2 -> develop
```


## License
See [LICENSE](LICENSE) ("BSD-2-Clause").  Attribution is appreciated but not required.
