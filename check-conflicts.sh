#! /usr/bin/env bash
#
# Check for upcoming merge conflicts in a Git project.
#
# Checks are performed a few different ways, with support for the following branching models:
# - GitHub Flow (master;<other>)
# - GitLab Flow (master;staging;<other>)
# - GitFlow (master;develop;release/*;hotfix/*;<other>)

# Documentation
read -r -d '' USAGESTRING <<USAGESTRING
Syntax: $(basename "$0") [VCS_HOST] [PROJECT/S]

Arguments:
	VCS_HOST	- required (single)
			The FQDN of the VCS projects' host.
	PROJECT/S	- required (single or multiple)
			The 'owner/project_name' of the projects to check.

Examples:
- $(basename "$0") gitlab.com robr3rd/merge-conflict-checker  # single project
- $(basename "$0") gitlab.com robr3rd/code-standards robr3rd/merge-conflict-checker robr3rd/notes  # multiple
USAGESTRING

function usage {
	echo "$USAGESTRING"
	# shellcheck disable=2086
	exit $1
}


# Platform-specific workaround for `wc` command
wcl() { wc -l; }  # GNU wc is fine
if [[ "$(uname)" == 'Darwin' ]]; then  # BSD/macOS includes leading spaces
	wcl() { wc -l | tr -d '[:space:]'; }  # strip spaces (also safe for GNU)
fi


# Global 'constants'
# (since this is Bash, we're throwing "variable scope" away but in a way that feels decidedly not-so-awful)
QTY_CHECKS=0
QTY_CONFLICTS=0


# Set project root
if [ "$ROOT_PATH" == '' ]; then
    ROOT_PATH="$(git rev-parse --show-toplevel)"
fi
tmp_folder="${ROOT_PATH}/tmp"


# Attempt to merge 'source_branch' and 'target_branch', checking for conflicts along the way.
# If conflicts are present, increment a counter and write the details of the attempt to a log file.
# _note: all variables are prefixed with 'f_' (for "function") to lazily simulate "local variable scope" in Bash ;)_
function check_conflict {
	f_source_branch="$1"  # Where from?
	f_target_branch="$2"  # Where to?
	f_project="$3"        # What context?   (optional)
	f_priority="$4"       # How important?  (optional)

	# These are optional, but if they exist they need the proper divider suffix
	[[ "$f_project" != '' ]] && f_project="${f_project}::"
	[[ "$f_priority" == '' ]] && f_priority='log' || [[ "$f_priority" != '' ]] && f_priority="${f_priority^^}::"

	echo "Merging '${f_source_branch}' into '${f_target_branch}'"  # announce current progress to stdout

	# Run the check
	git reset --hard "$f_target_branch"
	if [ "$(git merge "${f_source_branch}" --no-commit --no-ff | grep CONFLICT | wcl)" != "0" ]; then
		echo "$(date)::${f_priority}${f_project}${f_source_branch#'origin/'} -> ${f_target_branch#'origin/'}" \
			>> "${ROOT_PATH}/conflicts.log"
		((QTY_CONFLICTS++))
	fi
	((QTY_CHECKS++))

	# Reset after each attempt (harmless if no conflict existed...aside from  some extra noise in stdout)
	git merge --abort
}


# Parse arguments
if [ $# -lt 2 ]; then  # if <2 arguments
	usage 1
elif [[ "$1" == '' ]]; then
	usage 1
fi

vcs_host="$1"    # FQDN of VCS host
shift  # remove $1 from the args so that all remaining args are just projects
projects=("$@")  # array of Git repos


# Start checking!
for project in "${projects[@]}"; do
	# Intuit branching model/workflow                   syntax help: branch exists || branch absent
	[[ "$(git branch --remote | grep staging | wcl)" != '0' ]] && has_staging=1 || has_staging=0
	[[ "$(git branch --remote | grep develop | wcl)" != '0' ]] && has_develop=1 || has_develop=0
	[[ "$(git branch --remote | grep hotfix/ | wcl)" != '0' ]] && has_hotfixes=1 || has_hotfixes=0
	[[ "$(git branch --remote | grep release/ | wcl)" != '0' ]] && has_releases=1 || has_releases=0

	# Get started!
	echo -e "Checking ${project}"
	cd "$tmp_folder" || exit 1

	# Preparations (one-time)
	if [ ! -d "$project" ]; then
		clone_path="git@${vcs_host}:${project}.git"
		echo "Cloning '${clone_path}'..."
		git clone "$clone_path" "${project}"
	fi

	# Ensure refs are up-to-date
	cd "${tmp_folder}/${project}" || exit 1
	echo -e "\nUpdating refs..."
	git fetch --all --prune


	# Main logic
	echo -e "\nTesting all feature branches with production..."
	branches=$(git branch --remote | grep -Ev 'origin/(master|staging|develop|hotfix/|release/)')
	for branch in $branches; do
		check_conflict "$branch" origin/master "$project" WARN  # all dev work SHOULD be compatible with production
	done

	if [ $has_staging ]; then
		echo -e "\nTesting staging with production..."
		check_conflict origin/staging origin/master "$project" ALERT  # inability to deploy staging to prod is bad
	fi

	if [ $has_hotfixes ]; then
		echo -e "\nTesting all hotfix branches with production..."
		branches=$(git branch --remote | grep origin/hotfix/)
		for branch in $branches; do
			check_conflict "$branch" origin/master "$project" CRIT  # hotfix branches SHOULD merge into prod soon
		done
	fi

	if [ $has_releases ]; then
		echo -e "\nTesting all release branches with production..."
		branches=$(git branch --remote | grep origin/release/)
		for branch in $branches; do
			check_conflict "$branch" origin/master "$project" ALERT  # release branches MUST be merged into prod
		done
	fi

	if [ $has_develop ]; then
		echo -e "\nTesting production with development..."
		check_conflict origin/master origin/develop "$project" ALERT  # master MUST NOT be ahead of develop

		echo -e "\nTesting all feature branches with development..."
		branches=$(git branch --remote | grep -Ev 'origin/(master|staging|develop|hotfix/|release/)')
		for branch in $branches; do
			check_conflict "$branch" origin/develop "$project" INFO  # features should keep up with the dev branch
		done
	fi

	# Separate summary and each project by some whitespace
	echo -e "\n\n"
done


# Summarize results
message="Summary: ${QTY_CONFLICTS} of ${QTY_CHECKS} checks had conflicts!"  # ~40col width
exit_code=0  # no conflicts = "SUCCESS"

if [ $QTY_CONFLICTS -gt 0 ]; then
	message="${message}  (see 'conflicts.log' for details)"  # only say this if it's relevant  (~75 col width total)
	exit_code=1  # conflicts = "ERROR"
fi

echo "$message"
exit $exit_code
